﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Gomory.Core;

namespace Gomory;

public class MainForm : Form
{
    // Поля, в яких зберігається інформація про вихідні дані про задачу
    private readonly Fraction[,] _initialMatrixA = new Fraction[20, 20];
    private readonly Fraction[] _initialVectorB = new Fraction[20];
    private readonly Fraction[] _initialVectorC = new Fraction[20];
    private int _initialM = 3;
    private int _initialN = 3;
    private int _initialDirectionOfObjectiveFunction;

    // Поля, в яких зберігаються значення з форми заповнення вихідними даними
    private Fraction[] _inputVectorB = new Fraction[20];
    private Fraction[] _inputVectorC = new Fraction[20];
    private Fraction[,] _inputMatrixA = new Fraction[20, 20];
    private int _inputDirectionOfObjectiveFunction;
    private int _inputN = 3;
    private int _inputM = 3;

    // Поля для збереження інформації про ітерації - список симплекс-таблиць та поточна обрана ітерація
    private readonly List<Iteration> _iterations = new();
    private int _currentIteration = -1;

    // Поля-елементи графічного інтерфейсу
    private readonly IContainer _components = null;
    private Button _buttonNewProblem;
    private DataGridViewTextBoxColumn _column1;
    private DataGridViewTextBoxColumn _column2;
    private Label _label1;
    private Label _label2;
    private DataGridView _simplexTableGrid;
    private SplitContainer _splitContainer;
    private ListBox _IterationsListBox;

    public MainForm()
    {
        InitializeComponent();
    }

    // Метод для заповнення форми вихідними даними
    private void WriteInputData()
    {
        _initialN = _inputN;
        _initialM = _inputM;
        _initialDirectionOfObjectiveFunction = _inputDirectionOfObjectiveFunction;

        // Заповнення коефіцієнтів цільової функції, вільних членів та коефіцієнтів цільової функції
        for (var i = 0; i < _inputM; ++i)
        {
            _initialVectorB[i] = _inputVectorB[i];

            for (var j = 0; j < _inputN; ++j)
            {
                _initialVectorC[j] = _inputVectorC[j];
                _initialMatrixA[i, j] = _inputMatrixA[i, j];
            }
        }

        // Зміна знаків у коефіцієнтів цільової функції, якщо цільова функція прямує до максимуму
        if (_inputDirectionOfObjectiveFunction == 0)
        {
            for (var index = 0; index < _initialN; ++index)
            {
                _initialVectorC[index] = -_initialVectorC[index];
            }
        }
    }

    // Метод для перезаписування елементу графічного інтерфейсу, який відображає поточну симплекс-таблицю
    private void WriteIterationGrid()
    {
        // Отримуємо дані про поточну симплекс-таблицю
        var currentIteration = _iterations[_currentIteration];

        // Визначення кількості рядків та стовпців
        var rows = currentIteration.M + 1;
        var columns = currentIteration.N + currentIteration.M + 2;
        _simplexTableGrid.RowCount = rows;
        _simplexTableGrid.ColumnCount = columns - 1;

        // Заповнюємо перший рядок
        _simplexTableGrid.Columns[0].HeaderText = "A0";
        for (var i = 1; i < _simplexTableGrid.ColumnCount; ++i)
            _simplexTableGrid.Columns[i].HeaderText = "A" + i;

        // Заповнюємо перший стовпець
        for (var i = 0; i < _simplexTableGrid.RowCount - 1; ++i)
            _simplexTableGrid.Rows[i].HeaderCell.Value = "x" + currentIteration.GetBaseVectorById(i);

        // Перераховуємо відношення стовпця А0 до напрямного стовпця
        if (currentIteration.DirectionalColumn != -1)
        {
            currentIteration.UpdateMarks(currentIteration.DirectionalColumn);
        }

        _simplexTableGrid.Rows[_simplexTableGrid.RowCount - 1].HeaderCell.Value = currentIteration.DirectionOfObjectiveFunction == 0 ? "Fmax" : "Fmin";

        currentIteration.GetTable(_simplexTableGrid);
    }

    // Метод для виводу списку ітерацій, значень напрямного рядка, стовпця та цільової функції, бо інформації про те, що оптимального розв'язку немає
    private void WriteIterationsInformation()
    {
        _IterationsListBox.Items.Clear();

        for (var i = 0; i < _iterations.Count; ++i)
        {
            // Отримуємо поточну ітерцію зі списку всіх ітерація
            var iteration = _iterations[i];

            // Якщо напрямні рядок та стовпець є визначеними, то виводимо їх, інакше - лише номер ітерації
            if ((iteration.DirectionalColumn != -1) & (iteration.DirectionalRow != -1))
            {
                var iterationItems = _IterationsListBox.Items;
                var iterationInfo = new string[6];

                iterationInfo[0] = "Іт. ";
                var iterationNumber = i + 1;
                var iterationNumberString = iterationNumber.ToString();

                iterationInfo[1] = iterationNumberString;
                iterationInfo[2] = ": Напрямний рядок: x";

                var baseVectorValue = iteration.GetBaseVectorById(iteration.DirectionalRow);
                var baseVectorValueString = baseVectorValue.ToString();

                iterationInfo[3] = baseVectorValueString;
                iterationInfo[4] = ", Напрямний стовпець: A";

                var directionalColumn = iteration.DirectionalColumn.ToString();
                iterationInfo[5] = directionalColumn;

                var iterationString = string.Concat(iterationInfo);
                iterationItems.Add(iterationString);
            }
            else
            {
                var iterations = _IterationsListBox.Items;

                var iterationNumber = i + 1;
                var iterationString = $"Іт. {iterationNumber}";

                iterations.Add(iterationString);
            }

            // Якщо знайдено оптимальний нецілочисельний розв'язок, виводимо інформацію про це
            if (iteration.IsNonIntegerSolved)
            {
                _IterationsListBox.Items[_IterationsListBox.Items.Count - 1] += ": Нецілочисленне рішення. Використ. метод Гоморі.";
            }

            // Якщо задача необмежена, виводимо інформацію про це
            if (iteration.IsInfinity)
            {
                _IterationsListBox.Items[_IterationsListBox.Items.Count - 1] += ". Задача не обмежена!";
            }

            // Якщо задача не має рохв'яхків, виводимо інформацію про це
            if (iteration.IsUnsolvable)
            {
                _IterationsListBox.Items[_IterationsListBox.Items.Count - 1] += ". Задача немає розв'язків!";
            }

            // Якщо задачу вирішено виводимо розв'язок задачі
            if (iteration.IsSolved)
            {
                _IterationsListBox.Items[_IterationsListBox.Items.Count - 1] += ". Оптимальне рішення";
            }
        }

        // Визначення симплекс-таблиці, яка буде відображатись (поточна)
        _IterationsListBox.SelectedIndex = _currentIteration;
    }

    // Метод, який призначений розв'язати задачу методом Гоморі
    private void Solve()
    {
        try
        {
            var currentIteration = _iterations[_currentIteration];
            currentIteration.WriteTable(_simplexTableGrid);

            while (!currentIteration.IsSolved && !currentIteration.IsUnsolvable && !currentIteration.IsInfinity)
            {
                if (currentIteration.CheckInfinity())
                {
                    currentIteration.IsInfinity = true;
                    WriteIterationsInformation();
                }

                if (currentIteration.CheckUnsolvable())
                {
                    currentIteration.IsUnsolvable = true;
                    WriteIterationsInformation();
                }

                if (currentIteration.CheckSolved())
                {
                    if (currentIteration.CheckInteger())
                    {
                        currentIteration.IsSolved = true;
                        WriteIterationsInformation();
                    }
                    // Безпосередньо метод Гоморі
                    else
                    {
                        currentIteration.IsNonIntegerSolved = true;
                        currentIteration.DirectionalRow = currentIteration.GetRowIdWithMaxFractionalPart();

                        var newIteration = new Iteration(currentIteration.N, currentIteration.M);
                        newIteration.Recognize(currentIteration);
                        newIteration.WriteTable(_simplexTableGrid);

                        while (_IterationsListBox.SelectedIndex < _iterations.Count - 1)
                        {
                            _iterations.RemoveAt(_IterationsListBox.SelectedIndex + 1);
                        }

                        newIteration.AddConstraint();

                        _iterations.Add(newIteration);
                        _currentIteration = _iterations.Count - 1;

                        currentIteration = _iterations[_currentIteration];

                        WriteIterationGrid();
                        WriteIterationsInformation();

                        continue;
                    }
                }

                if (currentIteration.HasOptimalNextStep())
                {
                    var nextIteration = new Iteration(currentIteration.N, currentIteration.M);
                    nextIteration.Recognize(currentIteration);
                    nextIteration.WriteTable(_simplexTableGrid);

                    while (_IterationsListBox.SelectedIndex < _iterations.Count - 1)
                    {
                        _iterations.RemoveAt(_IterationsListBox.SelectedIndex + 1);
                    }

                    _iterations.Add(nextIteration);
                    _currentIteration = _iterations.Count - 1;

                    currentIteration = _iterations[_currentIteration];
                    ++nextIteration.DirectionalRow;

                    nextIteration.UpdateTable();

                    nextIteration.DirectionalRow = -1;
                    nextIteration.DirectionalColumn = -1;

                    WriteIterationGrid();
                    WriteIterationsInformation();
                }
            }
        }

        catch
        {
            Application.Exit();
        }
    }

    // Метод-подія, який спрацьовує при заповненні нової задачі, а саме при запуску форми або при кліку на кнопку створення нової задачі
    private void SolveProblemHandler(object sender, EventArgs e)
    {
        // Створення та відображення форми з кількістю обмжень та рівнянь
        var newProblemForm = new NewProblemForm();
        if (newProblemForm.ShowDialog() != DialogResult.OK)
            return;

        // Створення та відображення форми із заповненням вихідних цільової функції, коефіцієнтів вільних членів та обмежень
        var newProblemFillForm = new NewProblemFillForm();
        newProblemFillForm.GenerateInputs(newProblemForm.N, newProblemForm.M);

        if (newProblemFillForm.ShowDialog() == DialogResult.OK)
        {
            _iterations.Clear();

            // Зчитуємо вихідні дані
            _inputN = newProblemFillForm.N;
            _inputM = newProblemFillForm.M;
            _inputVectorB = newProblemFillForm.VectorB;
            _inputVectorC = newProblemFillForm.VectorC;
            _inputMatrixA = newProblemFillForm.MatrixA;
            _inputDirectionOfObjectiveFunction = newProblemFillForm.DirectionObjectiveFunction;

            // Записуємо вихідні дані у приватні поля форми
            WriteInputData();

            // Формуємо вихідну симплекс-таблицю
            var initialIteration = new Iteration(_initialN, _initialM);
            initialIteration.WriteTable(_initialMatrixA, _initialVectorB, _initialVectorC);
            initialIteration.DirectionOfObjectiveFunction = _initialDirectionOfObjectiveFunction;

            // Додаємо вихідну симплекс-таблицю до списку ітерацій та оноволюємо значення поточної ітерації
            _iterations.Add(initialIteration);
            _currentIteration = _iterations.Count - 1;

            // Оновлюємо відображення поточної симплекс-таблиці та вивід списку ітерацій
            WriteIterationGrid();
            WriteIterationsInformation();

            _splitContainer.Visible = true;
        }

        // Виклик методу, який виконує розв'язання задачі
        Solve();
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && _components != null)
            _components.Dispose();
        base.Dispose(disposing);
    }

    // Визначення елементів графічного інтерфейсу головної форми
    private void InitializeComponent()
    {
        var dataGridViewCellStyle1 = new DataGridViewCellStyle();
        dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle1.BackColor = SystemColors.Control;
        dataGridViewCellStyle1.Font = new Font("Century Gothic", 9.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
        dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;

        _splitContainer = new SplitContainer();
        _simplexTableGrid = new DataGridView();
        _column1 = new DataGridViewTextBoxColumn();
        _column2 = new DataGridViewTextBoxColumn();
        _label2 = new Label();
        _label1 = new Label();
        _buttonNewProblem = new Button();
        _IterationsListBox = new ListBox();
        _splitContainer.Panel1.SuspendLayout();
        _splitContainer.Panel2.SuspendLayout();
        _splitContainer.SuspendLayout();
        ((ISupportInitialize)_simplexTableGrid).BeginInit();
        SuspendLayout();

        _splitContainer.Dock = DockStyle.Fill;
        _splitContainer.Location = new Point(0, 0);
        _splitContainer.Name = "_splitContainer";
        _splitContainer.Orientation = Orientation.Horizontal;
        _splitContainer.Panel1.Controls.Add(_simplexTableGrid);
        _splitContainer.Panel2.BackColor = Color.FromArgb(255, 255, 192);
        _splitContainer.Panel2.Controls.Add(_label2);
        _splitContainer.Panel2.Controls.Add(_label1);
        _splitContainer.Panel2.Controls.Add(_buttonNewProblem);
        _splitContainer.Panel2.Controls.Add(_IterationsListBox);
        _splitContainer.Size = new Size(909, 567);
        _splitContainer.SplitterDistance = 331;
        _splitContainer.TabIndex = 6;
        _splitContainer.Visible = false;

        _simplexTableGrid.AllowUserToAddRows = false;
        _simplexTableGrid.AllowUserToDeleteRows = false;
        _simplexTableGrid.BackgroundColor = Color.RosyBrown;

        _simplexTableGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        _simplexTableGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        _simplexTableGrid.Columns.AddRange(_column1, _column2);
        _simplexTableGrid.Dock = DockStyle.Fill;
        _simplexTableGrid.GridColor = Color.FromArgb(192, 0, 192);
        _simplexTableGrid.Location = new Point(0, 0);
        _simplexTableGrid.MultiSelect = false;
        _simplexTableGrid.Name = "_simplexTableGrid";
        _simplexTableGrid.RowHeadersWidth = 75;
        _simplexTableGrid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        _simplexTableGrid.SelectionMode = DataGridViewSelectionMode.CellSelect;
        _simplexTableGrid.ShowCellErrors = false;
        _simplexTableGrid.ShowCellToolTips = false;
        _simplexTableGrid.ShowEditingIcon = false;
        _simplexTableGrid.ShowRowErrors = false;
        _simplexTableGrid.Size = new Size(909, 331);
        _simplexTableGrid.TabIndex = 5;
        _simplexTableGrid.ColumnHeadersDefaultCellStyle.BackColor = Color.LightCyan;
        _simplexTableGrid.ColumnHeadersDefaultCellStyle.ForeColor = Color.DarkCyan;
        _simplexTableGrid.ColumnHeadersDefaultCellStyle.Font = new Font("Century Gothic", 9.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
        _simplexTableGrid.RowHeadersDefaultCellStyle.BackColor = Color.LightPink;
        _simplexTableGrid.RowHeadersDefaultCellStyle.ForeColor = Color.Crimson;
        _simplexTableGrid.RowHeadersDefaultCellStyle.Font = new Font("Century Gothic", 9.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
        _simplexTableGrid.EnableHeadersVisualStyles = false;
        _simplexTableGrid.CellFormatting += (_, e) =>
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                e.CellStyle.BackColor = (e.RowIndex % 2 == 0) ? Color.Wheat : Color.YellowGreen;

                if (e.RowIndex == _simplexTableGrid.RowCount - 1)
                {
                    e.CellStyle.BackColor = Color.Crimson;
                }

                e.CellStyle.SelectionBackColor = Color.CadetBlue;
            }
        };
    
        _column1.HeaderText = "_column1";
        _column1.MinimumWidth = 6;
        _column1.Name = "_column1";
        _column1.Width = 125;

        _column2.HeaderText = "_column2";
        _column2.MinimumWidth = 6;
        _column2.Name = "_column2";
        _column2.Width = 125;

        _label2.AutoSize = true;
        _label2.ForeColor = Color.FromArgb(64, 0, 64);
        _label2.Location = new Point(602, 35);
        _label2.Name = "_label2";
        _label2.Size = new Size(279, 112);
        _label2.TabIndex = 10;
        _label2.Text = "Програма має призначення вирішувати\r\nзадачі цілочисельні задачі лінійного \r\nпрогр" +
                      "амування, при знаку ≤ у \r\nвхідних обмеженнях.\r\n\r\nМаксимальна кількість змінних: " +
                      "20\r\nМаксимальна кількість обмежень: 20";

        _label1.AutoSize = true;
        _label1.Font = new Font("Century Gothic", 12.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
        _label1.ForeColor = Color.Purple;
        _label1.Location = new Point(600, 5);
        _label1.Name = "_label1";
        _label1.Size = new Size(129, 19);
        _label1.TabIndex = 9;
        _label1.Text = "Метод Гоморі";
 
        _buttonNewProblem.BackColor = Color.LimeGreen;
        _buttonNewProblem.ForeColor = Color.White;
        _buttonNewProblem.Location = new Point(603, 165);
        _buttonNewProblem.Name = "_buttonNewProblem";
        _buttonNewProblem.Size = new Size(294, 34);
        _buttonNewProblem.TabIndex = 8;
        _buttonNewProblem.Text = "Розв\'язати нову задачу";
        _buttonNewProblem.UseVisualStyleBackColor = false;
        _buttonNewProblem.Click += SolveProblemHandler;

        _IterationsListBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom
                                                 | AnchorStyles.Left
                                                 | AnchorStyles.Right;
        _IterationsListBox.BackColor = Color.FromArgb(192, 255, 255);
        _IterationsListBox.ForeColor = Color.FromArgb(64, 0, 64);
        _IterationsListBox.FormattingEnabled = true;
        _IterationsListBox.HorizontalScrollbar = true;
        _IterationsListBox.ItemHeight = 16;
        _IterationsListBox.Location = new Point(1, 3);
        _IterationsListBox.Name = "_IterationsListBox";
        _IterationsListBox.Size = new Size(579, 196);
        _IterationsListBox.TabIndex = 7;
        _IterationsListBox.SelectedIndexChanged += (_, _) =>
        {
            _currentIteration = _IterationsListBox.SelectedIndex;
            WriteIterationGrid();
        };

        AutoScaleDimensions = new SizeF(8F, 16F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(255, 128, 128);
        ClientSize = new Size(909, 567);
        Controls.Add(_splitContainer);
        Font = new Font("Century Gothic", 9.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
        Name = "MainForm";
        Icon = new Icon("icon.ico");
        StartPosition = FormStartPosition.CenterScreen;
        Text = "Вирішення задач цілочисельного програмування методом Гоморі";
        _splitContainer.Panel1.ResumeLayout(false);
        _splitContainer.Panel2.ResumeLayout(false);
        _splitContainer.Panel2.PerformLayout();
        _splitContainer.ResumeLayout(false);
        ((ISupportInitialize)_simplexTableGrid).EndInit();
        ResumeLayout(false);
        Load += SolveProblemHandler;
        FormBorderStyle = FormBorderStyle.FixedSingle;
        MaximizeBox = false;
    }
}