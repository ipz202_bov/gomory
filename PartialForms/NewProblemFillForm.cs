﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Gomory.Core;
using Gomory.Helpers;

namespace Gomory;

public class NewProblemFillForm : Form
{
    // Властивості, які зберігають інформацію про вихідну модель
    public int N { get; private set; } = 3;
    public int M { get; private set; } = 3;
    public int DirectionObjectiveFunction => _directionObjectiveFunctionComboBox.SelectedIndex;

    public Fraction[,] MatrixA = new Fraction[20, 20];
    public Fraction[] VectorB = new Fraction[20];
    public Fraction[] VectorC = new Fraction[20];

    // Поля-елементи графічного інтерфейсу
    private Button _buttonCancel;
    private Button _buttonSolve;

    private readonly IContainer _components = null;

    private Label _labelMatrixA;
    private Label _labelVectorB;
    private Label _labelVectorC;

    private ComboBox _directionObjectiveFunctionComboBox;

    private Panel _panel;

    private DataGridView MatrixAGridView;
    private DataGridView VectorBGridView;
    private DataGridView VectorCGridView;

    public NewProblemFillForm()
    {
        InitializeComponent();
    }

    // Метод, який призначений малювати форми для введення цільової функції, вільних змінних та обмежень
    public void GenerateInputs(int n, int m)
    {
        N = n;
        M = m;

        MatrixAGridView.RowCount = M;
        MatrixAGridView.ColumnCount = N;

        MatrixAGridView.Height = MatrixAGridView.RowCount * MatrixAGridView.RowTemplate.Height + 3;
        MatrixAGridView.Width = MatrixAGridView.ColumnCount * 50;

        for (var i = 0; i < M; ++i)
        {
            var label = new Label();
            label.Parent = this;
            label.Text = "≤";
            label.Left = MatrixAGridView.Left + MatrixAGridView.Width + 35;
            label.Top = MatrixAGridView.Top + i * MatrixAGridView.RowTemplate.Height + 5;
        }

        _labelVectorC.Left = MatrixAGridView.Left + MatrixAGridView.Width + 80;

        VectorBGridView.Left = MatrixAGridView.Left + MatrixAGridView.Width + 80;
        VectorBGridView.Height = MatrixAGridView.RowCount * MatrixAGridView.RowTemplate.Height + 3;
        VectorBGridView.RowCount = M;
        VectorBGridView.ColumnCount = 1;

        _labelVectorB.Top = MatrixAGridView.Top + MatrixAGridView.Height + 35;

        VectorCGridView.Top = _labelVectorB.Top + _labelVectorB.Height + 5;
        VectorCGridView.RowCount = 1;
        VectorCGridView.ColumnCount = N;
        VectorCGridView.Width = VectorCGridView.ColumnCount * 50;
        VectorCGridView.Height = VectorCGridView.RowCount * VectorCGridView.RowTemplate.Height + 3;

        _panel.Top = VectorCGridView.Top + VectorCGridView.Height + 15;

        if (_labelVectorC.Width < VectorBGridView.Width)
        {
            _panel.Width = VectorBGridView.Left + VectorBGridView.Width - _panel.Left;
        }
        else
        {
            _panel.Width = _labelVectorC.Left + _labelVectorC.Width - _panel.Left;
        }

        Width = _panel.Left + _panel.Width + 15;
        Height = _panel.Top + _panel.Height + 70;

        // Ініціалізуємо всі згенеровані форми нулями
        DataGridViewHelper.Clear(MatrixAGridView);
        DataGridViewHelper.Clear(VectorBGridView);
        DataGridViewHelper.Clear(VectorCGridView);

        _directionObjectiveFunctionComboBox.Top = _buttonSolve.Top - 25;
        _directionObjectiveFunctionComboBox.SelectedIndex = 0;
    }

    // Метод призначений для валідування вхідних даних -  цільової функції, вільних змінних та обмежень
    private void ValidateInputData()
    {
        ((CultureInfo)CultureInfo.CurrentCulture.Clone()).NumberFormat.NumberDecimalSeparator = ",";

        for (var i = 0; i < MatrixAGridView.RowCount; ++i)
        {
            for (var j = 0; j < MatrixAGridView.ColumnCount; ++j)
            {
                try
                {
                    var value = MatrixAGridView[j, i].Value.ToString().Replace(".", ",");
                    MatrixA[i, j] = value;
                }
                catch
                {
                    _buttonSolve.DialogResult = DialogResult.None;
                    MessageBox.Show("Помилка при зчитувані коефіцієнтів обмежень", "Помилка введення", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        for (var i = 0; i < VectorBGridView.RowCount; ++i)
        {
            try
            {
                var value = VectorBGridView[0, i].Value.ToString().Replace(".", ",");
                VectorB[i] = value;
            }
            catch
            {
                _buttonSolve.DialogResult = DialogResult.None;
                MessageBox.Show("Помилка при зчитувані вектору вільних членів", "Помилка введення", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        for (var j = 0; j < VectorCGridView.ColumnCount; ++j)
        {
            try
            {
                var str = VectorCGridView[j, 0].Value.ToString().Replace(".", ",");
                VectorC[j] = str;
            }
            catch
            {
                _buttonSolve.DialogResult = DialogResult.None;
                MessageBox.Show("Помилка при зчитувані вектору коефіцієнтів цільової функції", "Помилка введення", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }

    // Метод-подія, який спрацьовує при кліку накнопку рішення задачі
    private void ButtonSolveClick(object sender, EventArgs e)
    {
        _buttonSolve.DialogResult = DialogResult.OK;

        // Перевіряємо дані на валідність
        ValidateInputData();

        if (_buttonSolve.DialogResult != DialogResult.OK)
            return;

        DialogResult = DialogResult.OK;
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && _components != null)
            _components.Dispose();
        base.Dispose(disposing);
    }

    // Визначення елементів графічного інтерфейсу форми
    private void InitializeComponent()
    {
        _labelMatrixA = new Label();
        _labelVectorB = new Label();
        _labelVectorC = new Label();

        VectorCGridView = new DataGridView();
        VectorBGridView = new DataGridView();
        MatrixAGridView = new DataGridView();

        _buttonCancel = new Button();
        _buttonSolve = new Button();

        _panel = new Panel();
        _panel.BorderStyle = BorderStyle.None;

        _directionObjectiveFunctionComboBox = new ComboBox();
        ((ISupportInitialize)MatrixAGridView).BeginInit();
        ((ISupportInitialize)VectorCGridView).BeginInit();
        ((ISupportInitialize)VectorBGridView).BeginInit();
        SuspendLayout();

        MatrixAGridView.AllowUserToAddRows = false;
        MatrixAGridView.AllowUserToDeleteRows = false;
        MatrixAGridView.AllowUserToResizeColumns = false;
        MatrixAGridView.AllowUserToResizeRows = false;
        MatrixAGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        MatrixAGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        MatrixAGridView.ColumnHeadersVisible = false;
        MatrixAGridView.Location = new Point(15, 25);
        MatrixAGridView.Name = "MatrixAGridView";
        MatrixAGridView.RowHeadersVisible = false;
        MatrixAGridView.RowHeadersWidth = 25;
        MatrixAGridView.ScrollBars = ScrollBars.Both;
        MatrixAGridView.ShowCellErrors = false;
        MatrixAGridView.ShowCellToolTips = false;
        MatrixAGridView.ShowEditingIcon = false;
        MatrixAGridView.ShowRowErrors = false;
        MatrixAGridView.Size = new Size(165, 64);
        MatrixAGridView.TabIndex = 0;
        MatrixAGridView.CellFormatting += (_, e) =>
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                e.CellStyle.BackColor = Color.Wheat;
                e.CellStyle.SelectionBackColor = Color.DarkSlateBlue;
            }
        };

        _labelMatrixA.AutoSize = true;
        _labelMatrixA.Location = new Point(12, 9);
        _labelMatrixA.Name = "_labelMatrixA";
        _labelMatrixA.Size = new Size(120, 13);
        _labelMatrixA.TabIndex = 1;
        _labelMatrixA.Text = "Обмеження:";
        _labelMatrixA.ForeColor = Color.DarkBlue;

        _labelVectorB.AutoSize = true;
        _labelVectorB.Location = new Point(12, 17);
        _labelVectorB.Name = "_labelVectorB";
        _labelVectorB.Size = new Size(144, 13);
        _labelVectorB.TabIndex = 2;
        _labelVectorB.Text = "Цільова функція:";
        _labelVectorB.ForeColor = Color.DarkRed;

        VectorCGridView.AllowUserToAddRows = false;
        VectorCGridView.AllowUserToDeleteRows = false;
        VectorCGridView.AllowUserToResizeColumns = false;
        VectorCGridView.AllowUserToResizeRows = false;
        VectorCGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        VectorCGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        VectorCGridView.ColumnHeadersVisible = false;
        VectorCGridView.Location = new Point(15, 163);
        VectorCGridView.Name = "VectorCGridView";
        VectorCGridView.RowHeadersVisible = false;
        VectorCGridView.ScrollBars = ScrollBars.None;
        VectorCGridView.ShowCellErrors = false;
        VectorCGridView.ShowCellToolTips = false;
        VectorCGridView.ShowEditingIcon = false;
        VectorCGridView.ShowRowErrors = false;
        VectorCGridView.Size = new Size(173, 49);
        VectorCGridView.TabIndex = 3;
        VectorCGridView.CellFormatting += (_, e) =>
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                e.CellStyle.BackColor = Color.Wheat;
                e.CellStyle.SelectionBackColor = Color.DarkRed;
            }
        };

        _labelVectorC.AutoSize = true;
        _labelVectorC.Location = new Point(299, 9);
        _labelVectorC.Name = "_labelVectorC";
        _labelVectorC.Size = new Size(101, 13);
        _labelVectorC.TabIndex = 4;
        _labelVectorC.Text = "Вільні члени:";
        _labelVectorC.ForeColor = Color.OrangeRed;

        VectorBGridView.AllowUserToAddRows = false;
        VectorBGridView.AllowUserToDeleteRows = false;
        VectorBGridView.AllowUserToResizeColumns = false;
        VectorBGridView.AllowUserToResizeRows = false;
        VectorBGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        VectorBGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        VectorBGridView.ColumnHeadersVisible = false;
        VectorBGridView.Location = new Point(302, 25);
        VectorBGridView.Name = "VectorBGridView";
        VectorBGridView.RowHeadersVisible = false;
        VectorBGridView.ScrollBars = ScrollBars.None;
        VectorBGridView.Size = new Size(75, 101);
        VectorBGridView.TabIndex = 5;
        VectorBGridView.CellFormatting += (_, e) =>
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                e.CellStyle.BackColor = Color.Wheat;
                e.CellStyle.SelectionBackColor = Color.DarkOrange;
            }
        };

        _buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _buttonCancel.DialogResult = DialogResult.Cancel;
        _buttonCancel.Location = new Point(350, 470);
        _buttonCancel.Name = "_buttonCancel";
        _buttonCancel.Size = new Size(75, 25);
        _buttonCancel.TabIndex = 6;
        _buttonCancel.Text = "Скасувати";
        _buttonCancel.UseVisualStyleBackColor = true;
        _buttonCancel.Font = new Font("Century Gothic", 7.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _buttonCancel.BackColor = Color.Red;
        _buttonCancel.ForeColor = Color.White;
        _buttonCancel.Click += (_, _) =>
        {
            Application.Exit();
        };

        _buttonSolve.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        _buttonSolve.Location = new Point(435, 470);
        _buttonSolve.Name = "_buttonSolve";
        _buttonSolve.Size = new Size(75, 25);
        _buttonSolve.TabIndex = 7;
        _buttonSolve.Text = "Розв'язати";
        _buttonSolve.UseVisualStyleBackColor = true;
        _buttonSolve.Click += ButtonSolveClick;
        _buttonSolve.Font = new Font("Century Gothic", 7.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _buttonSolve.BackColor = Color.Green;
        _buttonSolve.ForeColor = Color.White;

        _directionObjectiveFunctionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        _directionObjectiveFunctionComboBox.FormattingEnabled = true;
        _directionObjectiveFunctionComboBox.Items.AddRange(new object[]
        {
            "Максимізувати",
            "Mінімізувати"
        });
        _directionObjectiveFunctionComboBox.Location = new Point(16, 192);
        _directionObjectiveFunctionComboBox.Name = "_directionObjectiveFunctionComboBox";
        _directionObjectiveFunctionComboBox.Size = new Size(ClientSize.Width - 30, 21);
        _directionObjectiveFunctionComboBox.TabIndex = 9;
        _directionObjectiveFunctionComboBox.ForeColor = Color.Brown;
        _directionObjectiveFunctionComboBox.BackColor = Color.Bisque;
        
        _panel.Location = new Point(15, 108);
        _panel.Name = "_panel";
        _panel.Size = new Size(200, 0);
        _panel.TabIndex = 8;
        _panel.BackColor = Color.Transparent;

        AcceptButton = _buttonSolve;
        AutoScaleDimensions = new SizeF(6f, 13f);
        AutoScaleMode = AutoScaleMode.Font;
        CancelButton = _buttonCancel;
        ClientSize = new Size(522, 500);

        Controls.Add(_labelVectorC);
        Controls.Add(VectorCGridView);
        Controls.Add(_labelVectorB);
        Controls.Add(_labelMatrixA);
        Controls.Add(MatrixAGridView);
        Controls.Add(VectorBGridView);
        Controls.Add(_buttonCancel);
        Controls.Add(_buttonSolve);
        Controls.Add(_panel);
        Controls.Add(_directionObjectiveFunctionComboBox);

        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        ShowInTaskbar = false;
        Text = "Заповнення вихідних даних";
        BackColor = Color.Aquamarine;
        Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);

        ((ISupportInitialize)MatrixAGridView).EndInit();
        ((ISupportInitialize)VectorCGridView).EndInit();
        ((ISupportInitialize)VectorBGridView).EndInit();

        StartPosition = FormStartPosition.Manual;
        DesktopBounds = new Rectangle(
            (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2,
            (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2,
            Width,
            Height);
        ControlBox = false;

        ResumeLayout(false);
        PerformLayout();
    }
}