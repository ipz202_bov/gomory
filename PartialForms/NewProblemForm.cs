﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Gomory;

public class NewProblemForm : Form
{
    // Властивості, які зберігають інформацію про кількість обмежень та кількість змінних (N - змінні, M - обмеження)
    public int N => Convert.ToInt32(_textBoxN.Text);
    public int M => Convert.ToInt32(_textBoxM.Text);

    // Поля-елементи графічного інтерфейсу
    private Button _buttonCancel;
    private Button _buttonGenerate;

    private Label _labelM;
    private Label _labelN;

    private TextBox _textBoxM;
    private TextBox _textBoxN;

    private readonly IContainer _components = null;

    private GroupBox _groupBox;

    public NewProblemForm()
    {
        InitializeComponent();
    }

    // Метод-подія, який спрацьовує при кліку на кнопку створення нової моделі
    private void ButtonGenerateClick(object sender, EventArgs e)
    {
        if (!ValidateInput())
        {
            DialogResult = DialogResult.None;
        }
    }

    // Метод призначений для валідування введених кількості обмежень та кількості змінних (N - змінні, M - обмеження, максимум - 20)
    private bool ValidateInput()
    {
        if (!int.TryParse(_textBoxN.Text, out var n) || n is <= 0 or > 20)
        {
            MessageBox.Show("Неправильне значення для кількості змінних. Будь ласка, введіть натуральне число від 1 до 20.", "Помилка введення",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        if (!int.TryParse(_textBoxM.Text, out var m) || m is <= 0 or > 20)
        {
            MessageBox.Show("Неправильне значення для кількості обмежень. Будь ласка, введіть натуральне число від 1 до 20.", "Помилка введення",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        return true;
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && _components != null)
            _components.Dispose();
        base.Dispose(disposing);
    }

    // Визначення елементів графічного інтерфейсу форми
    private void InitializeComponent()
    {
        _buttonCancel = new Button();
        _buttonGenerate = new Button();
        _groupBox = new GroupBox();
        _textBoxM = new TextBox();
        _labelM = new Label();
        _labelN = new Label();
        _textBoxN = new TextBox();

        _groupBox.SuspendLayout();
        SuspendLayout();

        _buttonGenerate.DialogResult = DialogResult.OK;
        _buttonGenerate.Location = new Point(135, 80);
        _buttonGenerate.Name = "_buttonGenerate";
        _buttonGenerate.Size = new Size(100, 30);
        _buttonGenerate.TabIndex = 0;
        _buttonGenerate.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _buttonGenerate.Text = "Генерувати";
        _buttonGenerate.BackColor = Color.Green;
        _buttonGenerate.ForeColor = Color.White;
        _buttonGenerate.DialogResult = DialogResult.OK;
        _buttonGenerate.Click += ButtonGenerateClick;

        _buttonCancel.DialogResult = DialogResult.Cancel;
        _buttonCancel.Location = new Point(10, 80);
        _buttonCancel.Name = "_buttonCancel";
        _buttonCancel.Size = new Size(100, 30);
        _buttonCancel.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _buttonCancel.TabIndex = 1;
        _buttonCancel.Text = "Скасувати";
        _buttonCancel.BackColor = Color.Red;
        _buttonCancel.ForeColor = Color.White;
        _buttonCancel.Click += (_, _) =>
        {
            Application.Exit();
        };

        _groupBox.Controls.Add(_textBoxM);
        _groupBox.Controls.Add(_labelM);
        _groupBox.Controls.Add(_labelN);
        _groupBox.Controls.Add(_textBoxN);
        _groupBox.Location = new Point(12, 5);
        _groupBox.Name = "_groupBox";
        _groupBox.Size = new Size(240, 60);
        _groupBox.TabIndex = 6;
        _groupBox.TabStop = false;
        _groupBox.FlatStyle = FlatStyle.Standard;
        _groupBox.Paint += (sender, e) =>
        {
            var gfx = e.Graphics;
            var pp = sender as GroupBox;
            gfx.Clear(pp.BackColor);
            gfx.DrawString(pp.Text, pp.Font, new SolidBrush(pp.ForeColor), new PointF(7, 0));
        };

        _textBoxM.Location = new Point(125, 30);
        _textBoxM.Name = "_textBoxM";
        _textBoxM.Size = new Size(75, 20);
        _textBoxM.TabIndex = 9;
        _textBoxM.Text = "2";
        _textBoxM.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _textBoxM.BackColor = Color.Cornsilk;

        _labelM.AutoSize = true;
        _labelM.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _labelM.Location = new Point(120, 5);
        _labelM.Name = "_labelM";
        _labelM.Size = new Size(166, 16);
        _labelM.TabIndex = 8;
        _labelM.Text = "Обмеження:";
        _labelM.ForeColor = Color.Purple;

        _labelN.AutoSize = true;
        _labelN.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0); ;
        _labelN.Location = new Point(13, 5);
        _labelN.Name = "_labelN";
        _labelN.Size = new Size(148, 16);
        _labelN.TabIndex = 7;
        _labelN.Text = "Змінні:";
        _labelN.ForeColor = Color.Purple;

        _textBoxN.Location = new Point(18, 30);
        _textBoxN.Name = "_textBoxN";
        _textBoxN.Size = new Size(75, 20);
        _textBoxN.TabIndex = 6;
        _textBoxN.Text = "2";
        _textBoxN.Font = new Font("Century Gothic", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
        _textBoxN.BackColor = Color.Cornsilk;

        AcceptButton = _buttonGenerate;
        AutoScaleDimensions = new SizeF(6f, 13f);
        AutoScaleMode = AutoScaleMode.Font;
        CancelButton = _buttonCancel;
        ClientSize = new Size(245, 128);
        Controls.Add(_groupBox);
        Controls.Add(_buttonGenerate);
        Controls.Add(_buttonCancel);
        FormBorderStyle = FormBorderStyle.FixedDialog;

        MaximizeBox = false;
        MinimizeBox = false;
        ShowInTaskbar = false;

        StartPosition = FormStartPosition.Manual;
        DesktopBounds = new Rectangle(
            (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2,
            (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2,
            Width,
            Height);
        ControlBox = false;

        Text = "Нова задача";
        BackColor = Color.LightPink;

        ResumeLayout(false);
        _groupBox.ResumeLayout(false);
    }
}