﻿using System;
using System.Windows.Forms;

namespace Gomory
{
    public static class Program
    {
        public static MainForm MainForm;

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm = new MainForm();
            Application.Run(MainForm);
        }
    }
}