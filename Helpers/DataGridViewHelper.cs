﻿using System.Drawing;
using System.Windows.Forms;

namespace Gomory.Helpers;

public class DataGridViewHelper
{
    // Метод, який предназначений заповнювати елемент графічного інтерфейсу DataGridView нулями
    public static void Clear(DataGridView dataGridView)
    {
        for (var j = 0; j < dataGridView.ColumnCount; ++j)
        {
            for (var i = 0; i < dataGridView.RowCount; ++i)
            {
                dataGridView[j, i].Value = 0;
                dataGridView.BackColor = Color.Wheat;
            }
        }
    }
}