﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Gomory.Core;

// Клас, який предназначений для виконання операцій над конкретною симплекс-таблицею
public class Iteration
{
    // Публічні поля класу, у яких зберігається інформація - напрямні рядок та стовпець, напрямок цільової функції тощо
    public int DirectionalRow { get; set; } = -1;
    public int DirectionalColumn { get; set; } = -1;
    public int DirectionOfObjectiveFunction { get; set; }

    public bool IsSolved { get; set; } = false;
    public bool IsNonIntegerSolved { get; set; } = false;
    public bool IsUnsolvable { get; set; } = false;
    public bool IsInfinity { get; set; } = false;

    public int Columns { get; private set; }
    public int Rows { get; private set; }
    public int N { get; set; }
    public int M { get; set; }

    // Захищені поля класу,в яких зберігаються матриця елементів таблиці, базисний масив, масив розв'зяків.
    private readonly Fraction[,] _fullTable = new Fraction[20, 20];
    private readonly ArrayList _baseVector = new();
    private readonly double[] _marksVector = new double[20];
    private readonly double[] _rootVector = new double[20];
    private readonly Fraction[] _rootVectorFraction = new Fraction[20];

    // Конструктор класу, в якому ми обнуляємо симплекс-таблицю та задаємо їй початкові значення
    public Iteration(int n, int m)
    {
        for (var i = 0; i < _rootVector.Length; ++i)
        {
            _rootVectorFraction[i] = 0L;
        }

        for (var i = 0; i < _fullTable.GetLength(0); ++i)
        {
            for (var j = 0; j < _fullTable.GetLength(1); ++j)
            {
                _fullTable[i, j] = 0L;
            }
        }

        DefineTable(n, m);
    }

    // Метод, який призначений для додавання нового обмеження (використовується безпосередньо для методу Гоморі)
    public void AddConstraint()
    {
        {
            // Отримуємо ідентифікатор рядка з найбільшою дробовою частиною числа
            var rowIdWithMaxFractionalPart = GetRowIdWithMaxFractionalPart();

            ++M;

            Rows = M + 1;
            Columns = 1 + N + M;
            _baseVector.Add(N + M);

            _fullTable[M - 1, Columns - 1] = 0L;

            // Копіюємо значення рядка з індексом M-1 в рядок M
            for (var j = 0; j < Columns; ++j)
                _fullTable[M, j] = _fullTable[M - 1, j];

            var fractionalPartDenominator = _fullTable[rowIdWithMaxFractionalPart, 0].ToDecimal() == 0.0
                ? 1.0
                : _fullTable[rowIdWithMaxFractionalPart, 0].ToDecimal() / _fullTable[rowIdWithMaxFractionalPart, 0].ToDecimal();

            var integerPart = Math.Truncate(_fullTable[rowIdWithMaxFractionalPart, 0].ToDecimal());

            // Обчислюємо нове значення для елемента в рядку M-1 та стовпці 0
            _fullTable[M - 1, 0] = _fullTable[rowIdWithMaxFractionalPart, 0] - (long)integerPart;
            _fullTable[M - 1, 0] = _fullTable[M - 1, 0] * fractionalPartDenominator * -1;

            _fullTable[M - 1, Columns - 1] = 1L;

            // Обчислюємо нові значення для елементів в рядку M-1 (крім стовпця 0)
            for (var j = 1; j < Columns - 1; ++j)
            {
                if (_fullTable[rowIdWithMaxFractionalPart, j] >= 0L)
                {
                    var integerPartValue = Math.Truncate(_fullTable[rowIdWithMaxFractionalPart, j].ToDecimal());
                    _fullTable[M - 1, j] = -(_fullTable[rowIdWithMaxFractionalPart, j] - integerPartValue);
                }
                else
                {
                    var absoluteIntegerPartValue = Math.Abs(Math.Truncate(_fullTable[rowIdWithMaxFractionalPart, j].ToDecimal()));
                    _fullTable[M - 1, j] = -(_fullTable[rowIdWithMaxFractionalPart, j] + absoluteIntegerPartValue + 1);
                }
            }
        }
    }

    // Метод, який визначає індекс рядка з найбільшою добовою частиною (використовується під час побудови відсічення у методі Гоморі)
    public int GetRowIdWithMaxFractionalPart()
    {
        // Змінна для зберігання ідентифікатора рядка з найбільшою дробовою частиною числа
        var id = -1;

        // Змінна для зберігання найбільшої дробової частини числа
        var maxFractionalPart = 0.0;

        for (var i = 0; i < M; ++i)
        {
            var fractionalPart = Math.Abs(_fullTable[i, 0].ToDecimal()) - Math.Abs(Math.Floor(_fullTable[i, 0].ToDecimal()));
            
            if (maxFractionalPart < fractionalPart)
            {
                maxFractionalPart = fractionalPart;
                id = i;
            }
        }

        return id;
    }

    // Метод, який призначений визначати кількість рядків та стовпців симплекс таблиці, а також заповнити базисний вектор
    public void DefineTable(int n, int m)
    {
        N = n;
        M = m;

        Rows = M + 1;
        Columns = 1 + N + M;

        DirectionOfObjectiveFunction = 0;

        InitFullTable();
        _baseVector.Clear();

        for (var i = 0; i < M; ++i)
        {
            _baseVector.Add(N + i + 1);
        }
    }

    // Метод, який повертає значення в базисному векторі по його індексу
    public int GetBaseVectorById(int id)
    {
        return (int)_baseVector[id];
    }

    // Метод, який виконує перевірку на нескінченність розв'язків
    public bool CheckInfinity()
    {
        var i = 0;
        int j, k;

        var flag = false;

        var num = _fullTable[i, 0].ToDecimal();

        for (j = 0; j < M; ++j)
        {
            if ((Math.Abs(num) < Math.Abs(_fullTable[j, 0].ToDecimal())) && (_fullTable[j, 0] < 0L))
            {
                num = _fullTable[j, 0].ToDecimal();
            }
        }

        if (num >= 0.0)
        {
            for (var l = 1; l <= N + M; ++l)
            {
                if (!IsInBase(l) && (_fullTable[M, l] < 0L))
                {
                    flag = true;

                    for (k = 0; k < M; ++k)
                    {
                        if (_fullTable[k, l] > 0L)
                        {
                            flag = false;
                        }
                    }

                    if (flag)
                    {
                        DirectionalRow = -1;
                        DirectionalColumn = l;
                        return flag;
                    }
                }
            }
        }

        return flag;
    }

    // Метод, який привоює значення поточної симплекс-таблиці тій, як приходить прааметром
    public void Recognize(Iteration iteration)
    {
        DirectionalColumn = iteration.DirectionalColumn;
        DirectionalRow = iteration.DirectionalRow;
        DirectionOfObjectiveFunction = iteration.DirectionOfObjectiveFunction;

        for (var index = 0; index < M; ++index)
        {
            _baseVector[index] = iteration.GetBaseVectorById(index);
        }
    }

    // Метод, який перевіряє чи є стовпчик меншим за нуль
    public bool IsColumnValid(double value)
    {
        return value < 0.0;
    }

    // Метод, який перевіряє чи ця змінна в базисі
    public bool IsInBase(int value)
    {
        var result = false;
        for (var i = 0; i < M; ++i)
        {
            if ((int)_baseVector[i] == value)
            {
                result = true;
            }
        }

        return result;
    }

    // Метод, який виконує перерахунок оцінок за якими ми визначаємо напрямний рядок (і навпаки в методі Гоморі)
    public void UpdateMarks(int directionalColumn)
    {
        if (directionalColumn == -1)
            return;

        for (var i = 0; i < M; ++i)
        {
            var isNegativeProduct = _fullTable[i, 0] * _fullTable[i, directionalColumn] < 0L;
            var isFirstColumnZero = _fullTable[i, 0] == 0;
            var isDirectionalColumnZero = _fullTable[i, directionalColumn] == 0;

            _marksVector[i] = isNegativeProduct || (isFirstColumnZero & isDirectionalColumnZero)
                ? -1.0
                : Math.Abs(_fullTable[i, 0].ToDecimal() / _fullTable[i, directionalColumn].ToDecimal());
        }
    }

    // Метод, який визначає наступні напрямні рядок та стовпець і визначає чи можливим є наступний крок
    public bool HasOptimalNextStep()
    {
        var enteringColumn = -1;
        var directionalColumn = -1;
        var hasOptimalStep = false;

        var enteringColumnValue = 0.0;
        var maxObjectiveValue = 0.0;

        for (var i = 0; i < M; i++)
        {
            if (_fullTable[i, 0] < 0L)
            {
                enteringColumnValue = _fullTable[i, 0].ToDecimal();
                enteringColumn = i;
            }
        }

        if (enteringColumn != -1)
        {
            for (var i = 0; i < M; i++)
            {
                if ((Math.Abs(enteringColumnValue) < Math.Abs(_fullTable[i, 0].ToDecimal())) && (_fullTable[i, 0] < 0L))
                {
                    enteringColumnValue = _fullTable[i, 0].ToDecimal();
                    enteringColumn = i;
                }
            }

            if (enteringColumnValue < 0.0)
            {
                var directionalColumnValue = 0.0;

                for (var j = 1; j < Columns; j++)
                {
                    if ((Math.Abs(directionalColumnValue) < Math.Abs(_fullTable[enteringColumn, j].ToDecimal())) && (_fullTable[enteringColumn, j].ToDecimal() < 0.0))
                    {
                        directionalColumnValue = _fullTable[enteringColumn, j].ToDecimal();
                        directionalColumn = j;
                    }
                }
            }
        }

        if (directionalColumn > 0)
        {
            UpdateMarks(directionalColumn);

            DirectionalRow = enteringColumn;
            DirectionalColumn = directionalColumn;

            return true;
        }

        var leavingRow = -1;

        for (var j = 1; j < Columns; j++)
        {
            var variable = j;
            if (IsColumnValid(_fullTable[M, variable].ToDecimal()) && !IsInBase(variable))
            {
                var directionalColumnIndex = variable;
                UpdateMarks(directionalColumnIndex);

                var maxMark = -1.0;

                for (var i = 0; i < M; i++)
                {
                    if (_marksVector[i] >= 0.0)
                    {
                        if (maxMark < 0.0)
                        {
                            leavingRow = i;
                            maxMark = _marksVector[leavingRow];
                        }
                        else if (maxMark > _marksVector[i])
                        {
                            leavingRow = i;
                            maxMark = _marksVector[leavingRow];
                        }
                    }
                }

                if ((maxMark >= 0.0) && (leavingRow != -1) && (directionalColumnIndex != -1))
                {
                    var directionalElementValue = _fullTable[leavingRow, directionalColumnIndex].ToDecimal();
                    var newObjectiveValue = _fullTable[M, 0].ToDecimal() + _fullTable[leavingRow, 0].ToDecimal() / directionalElementValue * (-1.0 * _fullTable[M, directionalColumnIndex].ToDecimal());

                    if (DirectionOfObjectiveFunction == 1)
                    {
                        newObjectiveValue = -newObjectiveValue;
                    }
                    
                    if (((maxObjectiveValue < newObjectiveValue) && (DirectionOfObjectiveFunction == 0)) || ((maxObjectiveValue > newObjectiveValue) && (DirectionOfObjectiveFunction == 1)))
                    {
                        hasOptimalStep = true;
                        DirectionalRow = leavingRow;
                        DirectionalColumn = directionalColumnIndex;
                        maxObjectiveValue = newObjectiveValue;
                    }
                }
                else
                {
                    DirectionalRow = -1;
                    DirectionalColumn = -1;
                    return false;
                }
            }
        }

        if (DirectionalColumn != -1)
        {
            UpdateMarks(DirectionalColumn);
        }

        return hasOptimalStep;
    }

    // Метод, який ініціалізує значеннями таблицю, враховуючи введені вільні змінні з коефіцієнтом "1"
    private void InitFullTable()
    {
        for (var i = 0; i < Rows; ++i)
        {
            for (var j = 0; j < Columns; ++j)
            {
                _fullTable[i, j] = 0L;
            }
        }

        for (var i = 0; i < Rows; ++i)
        {
            for (var j = N + 1; j < Columns; ++j)
            {
                if (i + N + 1 == j)
                {
                    _fullTable[i, j] = 1L;
                }
            }
        }
    }
    
    // Метод, який призначений для заповнення елементу графічного інтерфейсу значеннями з таблиці
    public void GetTable(DataGridView grid)
    {
        for (var i = 0; i < Rows; ++i)
        {
            for (var j = 0; j < Columns; ++j)
            {
                grid[j, i].Value = _fullTable[i, j].ToString();
            }
        }
    }

    // Метод, який призначений для заповнення таблиці, зчитуючи дані з елементу графічного інтерфейсу
    public void WriteTable(DataGridView grid)
    {
        for (var i = 0; i < Rows; ++i)
        {
            for (var j = 0; j < Columns; ++j)
            {
                try
                {
                    _fullTable[i, j] = grid[j, i].Value.ToString();
                }
                catch
                {
                    throw new InvalidOperationException("Не вдалось зчитати дані із симплекс-таблиці");
                }
            }
        }
    }

    // Метод, який призначений для заповнення таблиці зі вхідних масивів, що описують математичну модель
    public void WriteTable(Fraction[,] matrixA, Fraction[] vectorB, Fraction[] vectorC)
    {
        for (var i = 0; i < M; ++i)
        {
            for (var j = 0; j < N; ++j)
            {
                _fullTable[i, j + 1] = matrixA[i, j];
            }
        }

        for (var i = 1; i < N + 1; ++i)
        {
            _fullTable[M, i] = vectorC[i - 1];
        }

        for (var i = 0; i < M; ++i)
        {
            _fullTable[i, 0] = vectorB[i];
        }
    }

    // Метод, який виконує перевірку на те, чи можливо розв'язати задачу
    public bool CheckUnsolvable()
    {
        var isUnsolvable = false; 
        var rowWithSmallestNegativeValue = -1;
        var smallestNegativeValue = 0.0;

        for (var i = 0; i < M; ++i)
        {
            if (_fullTable[i, 0] < 0L)
            {
                smallestNegativeValue = _fullTable[i, 0].ToDecimal();
                rowWithSmallestNegativeValue = i;
            }
        }

        if (rowWithSmallestNegativeValue != -1)
        {
            for (var i = 0; i < M; ++i)
            {
                if ((Math.Abs(smallestNegativeValue) < Math.Abs(_fullTable[i, 0].ToDecimal())) & (_fullTable[i, 0] < 0L))
                {
                    smallestNegativeValue = _fullTable[i, 0].ToDecimal();
                    rowWithSmallestNegativeValue = i;
                }
            }

            if (smallestNegativeValue < 0.0)
            {
                isUnsolvable = true;
                var smallestValueInRow = 0.0;
                ++DirectionalRow;

                for (var i = 1; i < Columns; ++i)
                {
                    if ((Math.Abs(smallestValueInRow) < Math.Abs(_fullTable[rowWithSmallestNegativeValue, i].ToDecimal())) &
                        (_fullTable[rowWithSmallestNegativeValue, i] < 0L))
                    {
                        DirectionalRow = -1;
                        isUnsolvable = false;
                        break;
                    }
                }
            }
        }

        return isUnsolvable;
    }

    // Метод, який виконує перевірку чи є розв'язки цілочисельними
    public bool CheckInteger()
    {
        var result = true;
        for (var i = 0; i < M; ++i)
        {
            if ((int)_baseVector[i] <= N && _fullTable[i, 0].Denominator != 1L)
            {
                result = false;
            }
        }

        return result;
    }

    // Метод, який виконує перевірку чи знайдено оптимальне нецілочисльне рішення
    public bool CheckSolved()
    {
        var result = true;
        for (var i = 1; i < Columns; ++i)
        {
            if (_fullTable[M, i] < 0L)
            {
                result = false;
            }
        }

        for (var i = 0; i < M; ++i)
        {
            if (_fullTable[i, 0] < 0L)
            {
                result = false;
            }
        }
            
        return result;
    }

    // Метод, який викнує перерахунок симплекс-таблиці
    public void UpdateTable()
    {
        var rowFraction = new Fraction(_fullTable[DirectionalRow - 1, DirectionalColumn]);

        for (var index = 0; index < Columns; ++index)
            _fullTable[DirectionalRow - 1, index] /= rowFraction;

        for (var i = 0; i < Rows; ++i)
        {
            if (i != DirectionalRow - 1)
            {
                var currentRowFraction = new Fraction(_fullTable[i, DirectionalColumn]);
                for (var j = 0; j < Columns; ++j)
                    _fullTable[i, j] -= _fullTable[DirectionalRow - 1, j] * currentRowFraction;
            }
        }

        _baseVector[DirectionalRow - 1] = DirectionalColumn;

        for (var i = 0; i < N; ++i)
        {
            _rootVector[i] = 0.0;
            _rootVectorFraction[i] = 0L;
        }

        for (var i = 0; i < M; ++i)
        {
            if ((int)_baseVector[i] <= N)
            {
                _rootVector[(int)_baseVector[i] - 1] = _fullTable[i, 0].ToDecimal();
                _rootVectorFraction[(int)_baseVector[i] - 1] = _fullTable[i, 0];
            }
        }
    }
}