﻿using System;
using System.Globalization;

namespace Gomory.Core;

// Клас, який предназначений зберігати звичайні дроби та виконувати операції над ними
public class Fraction
{
    // Властивість для збереження чисельника
    public long Numerator { get; set; }

    // Поле та властивість для збереження знаменника
    private long _denominator;
    public long Denominator
    {
        get => _denominator;
        set => _denominator = value != 0L
            ? value
            : throw new ArgumentException("The denominator cannot be zero");
    }

    // Конструктори класу, які дозволяють привести дріб будь-якого формату(числового, рядкового) до формату звичайного дробу
    public Fraction()
    {
        Init(0L, 1L);
    }

    public Fraction(long numerator)
    {
        Init(numerator, 1L);
    }

    public Fraction(double doubleFraction)
    {
        var fraction = ToFraction(doubleFraction);

        Init(fraction.Numerator, fraction.Denominator);
    }

    public Fraction(string stringFraction)
    {
        var fraction = ToFraction(stringFraction);

        Init(fraction.Numerator, fraction.Denominator);
    }

    public Fraction(long numerator, long denominator)
    {
        Init(numerator, denominator);
    }

    // Метод, який призначений приводити дріб з форматку звичайного дробу до десяткового формату
    public double ToDecimal()
    {
        var value = Numerator / (double)Denominator;
        return value;
    }

    // Метод, який призначений приводити дріб з форматку звичайного дробу до рядкового формату
    public override string ToString()
    {
        return Denominator == 1L
            ? $"{Numerator}"
            : $"{Numerator}/{Denominator}";
    }

    // Метод, який виконує обернення дробу, тобто змінює місцями чисельник та знаменник
    public static Fraction Inverse(Fraction fraction)
    {
        if (fraction.Numerator == 0L)
            throw new ArgumentException("The denominator cannot be zero");

        return new Fraction(fraction.Denominator, fraction.Numerator);
    }

    // Метод, який виконує додавання дробів
    private static Fraction Add(Fraction firstFraction, Fraction secondFraction)
    {
        try
        {
            var numerator = checked(firstFraction.Numerator * secondFraction.Denominator +
                                    secondFraction.Numerator * firstFraction.Denominator);
            var denominator = checked(firstFraction.Denominator * secondFraction.Denominator);

            return new Fraction(numerator, denominator);
        }
        catch
        {
            throw new InvalidOperationException($"Could not add fractions {firstFraction} and {secondFraction}");
        }
    }

    // Метод, який виконує множення дробів
    private static Fraction Multiply(Fraction firstFraction, Fraction secondFraction)
    {
        try
        {
            var numerator = checked(firstFraction.Numerator * secondFraction.Numerator);
            var denominator = checked(firstFraction.Denominator * secondFraction.Denominator);

            return new Fraction(numerator, denominator);
        }
        catch
        {
            throw new InvalidOperationException($"Could not multiply fractions {firstFraction} and {secondFraction}");
        }
    }

    // Метод, який виконує зміну знаку у дроба на протилежний (2/3 => -2/3, -5/6 => 5/6)
    private static Fraction ChangeSign(Fraction fraction)
    {
        return new Fraction(-fraction.Numerator, fraction.Denominator);
    }

    // Метод, який виконує приведення десяткового дробу до звичайного
    public static Fraction ToFraction(double doubleFraction)
    {
        try
        {
            Fraction fraction;

            // Перевірка, чи число є цілим
            if (doubleFraction % 1.0 == 0.0)
            {
                fraction = new Fraction(checked((long)doubleFraction));
                return fraction;
            }

            var currentValue = doubleFraction;
            long denominator = 1;

            // Цикл для перетворення числа з десятковго представлення в рядкове представлення
            string stringValue;
            for (stringValue = doubleFraction.ToString(CultureInfo.InvariantCulture); stringValue.IndexOf("E", StringComparison.Ordinal) > 0; stringValue = currentValue.ToString(CultureInfo.InvariantCulture))
            {
                currentValue *= 10.0;
                checked { denominator *= 10L; }
            }

            var decimalSeparatorIndex = 0;
            var decimalString = stringValue.Replace(",", ".");

            // Знаходження індексу десяткового роздільника
            while (decimalString[decimalSeparatorIndex] != '.')
                checked { ++decimalSeparatorIndex; }

            var decimalPlaces = checked(decimalString.Length - decimalSeparatorIndex - 1);

            // Цикл для перетворення десяткової частини в знаменник дробу
            while (decimalPlaces > 0)
            {
                currentValue *= 10.0;
                checked { denominator *= 10L; }
                checked { --decimalPlaces; }
            }

            // Створення об'єкту типу Fraction з отриманим чисельником та знаменником
            fraction = new Fraction(checked((long)Math.Round(currentValue)), denominator);
            return fraction;
        }
        catch
        {
            throw new ArgumentException($"Cannot parse {doubleFraction} to fraction");
        }
    }

    // Метод, який виконує приведення дробу у рядковому форматі до звичайного
    public static Fraction ToFraction(string fraction)
    {
        var slashIndex = fraction.IndexOf('/');
        if (slashIndex >= 0)
        {
            var numerator = Convert.ToInt64(fraction.Substring(0, slashIndex));
            var denominator = Convert.ToInt64(fraction.Substring(slashIndex + 1));

            return new Fraction(numerator, denominator);
        }

        var cultures = new[]
        {
                CultureInfo.InvariantCulture,
                CultureInfo.GetCultureInfo("uk-UA")
            };

        foreach (var culture in cultures)
        {
            if (double.TryParse(fraction, NumberStyles.Float, culture, out var doubleValue))
            {
                var resultFraction = ToFraction(doubleValue);

                return new Fraction(resultFraction.Numerator, resultFraction.Denominator);
            }
        }

        throw new ArgumentException($"Invalid fraction format: {fraction}");
    }

    // Метод, який предназначений для скорочення звичайного дробу
    public static void Reduce(Fraction fraction)
    {
        try
        {
            if (fraction.Numerator != 0L)
            {
                // Обчислення найбільшого спільного дільника чисельника та знаменника
                var greatestCommonDivisor = CalculateGcd(fraction.Numerator, fraction.Denominator);

                // Скорочення дробу за допомогою найбільшого спільного дільника
                fraction.Numerator /= greatestCommonDivisor;
                fraction.Denominator /= greatestCommonDivisor;

                // Перевірка на від'ємний знаменник та нормалізація дробу
                if (fraction.Denominator < 0L)
                {
                    fraction.Numerator *= -1L;
                    fraction.Denominator *= -1L;
                }
            }
            else
            {
                // Якщо чисельник рівний нулю, знаменник встановлюється на 1
                fraction.Denominator = 1L;
            }
        }
        catch
        {
            throw new InvalidOperationException($"Fraction {fraction} could not be reduced");
        }
    }

    // Метод, який призначений ініціалізувати властивості чисельника та знаменника, а також виконуємо скорочення дробу
    private void Init(long numerator, long denominator)
    {
        Numerator = numerator;
        Denominator = denominator;

        Reduce(this);
    }

    // Знаходження НСД (найбільшого спільного дільника) двох чисел за алгоритмом Евкліда
    private static long CalculateGcd(long number1, long number2)
    {
        // Перевірка на від'ємність та встановлення додатніх значень
        if (number1 < 0L)
            number1 = -number1;
        if (number2 < 0L)
            number2 = -number2;

        // Алгоритм Евкліда для обчислення найбільшого спільного дільника
        do
        {
            // Перестановка чисел
            if (number1 < number2)
            {
                var temp = number1;
                number1 = number2;
                number2 = temp;
            }

            // Знаходження остачі від ділення number1 на number2
            number1 %= number2;
        }
        while (number1 != 0L);

        // Повернення найбільшого спільного дільника
        return number2;
    }

    // Метод, який призначений перевіряти чи рівний дріб іншому дробові
    protected bool Equals(Fraction fraction)
    {
        return _denominator == fraction._denominator && Numerator == fraction.Numerator;
    }

    // Виконуємо перевантаження операторів (можливість мови С#, для того, щоб виконувати операції над об'єктами класу так як зі звичайними числами)
    public static Fraction operator +(Fraction fraction, long number)
    {
        return Add(fraction, new Fraction(number));
    }

    public static Fraction operator +(Fraction fraction, double number)
    {
        return Add(fraction, ToFraction(number));
    }

    public static Fraction operator -(Fraction firstFraction, Fraction secondFraction)
    {
        return Add(firstFraction, -secondFraction);
    }

    public static Fraction operator -(Fraction fraction, long number)
    {
        return Add(fraction, -new Fraction(number));
    }

    public static Fraction operator -(Fraction fraction, double number)
    {
        return Add(fraction, -ToFraction(number));
    }

    public static Fraction operator -(Fraction fraction)
    {
        return ChangeSign(fraction);
    }

    public static Fraction operator *(Fraction firstFraction, Fraction secondFraction)
    {
        return Multiply(firstFraction, secondFraction);
    }

    public static Fraction operator *(Fraction fraction, long number)
    {
        return Multiply(fraction, new Fraction(number));
    }

    public static Fraction operator *(Fraction fraction, double number)
    {
        return Multiply(fraction, ToFraction(number));
    }

    public static Fraction operator /(Fraction firstFraction, Fraction secondFraction)
    {
        return Multiply(firstFraction, Inverse(secondFraction));
    }

    public static bool operator ==(Fraction firstFraction, Fraction secondFraction)
    {
        return firstFraction.Equals(secondFraction);
    }

    public static bool operator ==(Fraction firstFraction, long number)
    {
        return firstFraction.Equals(new Fraction(number));
    }

    public static bool operator ==(Fraction firstFraction, double number)
    {
        return firstFraction.Equals(ToFraction(number));
    }

    public static bool operator !=(Fraction firstFraction, Fraction secondFraction)
    {
        return !firstFraction.Equals(secondFraction);
    }

    public static bool operator !=(Fraction firstFraction, long number)
    {
        return !firstFraction.Equals(new Fraction(number));
    }

    public static bool operator !=(Fraction firstFraction, double number)
    {
        return !firstFraction.Equals(ToFraction(number));
    }

    public static bool operator <(Fraction firstFraction, Fraction secondFraction)
    {
        return firstFraction.Numerator * firstFraction.Denominator < secondFraction.Numerator * secondFraction.Denominator;
    }

    public static bool operator >(Fraction firstFraction, Fraction secondFraction)
    {
        return firstFraction.Numerator * firstFraction.Denominator > secondFraction.Numerator * secondFraction.Denominator;
    }

    public static bool operator <=(Fraction firstFraction, Fraction secondFraction)
    {
        return firstFraction.Numerator * firstFraction.Denominator <= secondFraction.Numerator * secondFraction.Denominator;
    }

    public static bool operator >=(Fraction firstFraction, Fraction secondFraction)
    {
        return firstFraction.Numerator * firstFraction.Denominator >= secondFraction.Numerator * secondFraction.Denominator;
    }

    // Визначення явних та неявних перетворень
    public static implicit operator Fraction(long number)
    {
        return new Fraction(number);
    }

    public static implicit operator Fraction(double number)
    {
        return new Fraction(number);
    }

    public static implicit operator Fraction(string fraction)
    {
        return new Fraction(fraction);
    }

    public static explicit operator double(Fraction fraction)
    {
        return fraction.ToDecimal();
    }

    public static implicit operator string(Fraction fraction)
    {
        return fraction.ToString();
    }
}